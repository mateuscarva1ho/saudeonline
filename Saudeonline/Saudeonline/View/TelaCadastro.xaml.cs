﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Saudeonline.View
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TelaCadastro : ContentPage
    {
        public Usuario Usuario { get; set; }
        public TelaCadastro()
        {
            InitializeComponent();
        }
        private void Button_Cadastrar_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine("=================");
            Console.WriteLine(this.Usuario.Nome);
            Console.WriteLine(this.Usuario.CPF);
            Console.WriteLine(this.Usuario.Idade);
            Console.WriteLine(this.Usuario.Telefone);
            Console.WriteLine(this.Usuario.Endereco);
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);

            
        }
    }
}