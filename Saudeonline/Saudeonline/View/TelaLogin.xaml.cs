﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Saudeonline.View
{

    public class Usuario
    {
        public string Nome { get; set; }
        public string Email { get; set; }
        public string Telefone { get; set; }
        public int CPF { get; set; }
        public int Idade { get; set; }
        public string Endereco { get; set; }
        public string Senha { get; set; }
    }
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class TelaLogin : ContentPage
    {
        public Usuario Usuario { get; set; }
        public TelaLogin()
        {
            InitializeComponent();
            this.Usuario = new Usuario();

            this.BindingContext = this;


        }
        private void Button_Login_Clicked(object sender, EventArgs e)
        {
            Console.WriteLine("=================");
            Console.WriteLine(this.Usuario.Email);
            Console.WriteLine(this.Usuario.Senha);

            ActivityIndicatorLogin.IsRunning = true;
            //SignIn();
        }

        private void Button_Cadastrar_Clicked(object sender, EventArgs e)
        {
            Navigation.PushAsync(new TelaCadastro());
        }
    }
}