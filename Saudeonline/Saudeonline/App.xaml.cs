﻿using Saudeonline.View;
using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;

namespace Saudeonline
{
    public partial class App : Application
    {
        public App()
        {
            InitializeComponent();

            MainPage = new NavigationPage(
                new TelaLogin());
        }

        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
